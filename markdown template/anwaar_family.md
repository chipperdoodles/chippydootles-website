+++
title = "Amulets"
description = "Circle shaped amulet"
draft = false
weight = 
sort_by = "weight"
+++

# Overview

## Family Specifications
* Size: 
* MCU: Mcu location on 
* Pins: 
* Programming: 
* Charging:

---

# Base Boards

## A-TINY-15F
{{ gallery(id="") }}


### Specifications
* MCU:
* Pins:
* Programming:
* Charging:
* Energy Storage: 
* VDD:

### Pin Out

{{ gallery(id="") }}

Pin out for 

| Pin  | Port | PWM[^1] | Analog[^2] | PTC[^3] |          Other            |
|------|:----:|:-------:|:----------:|:-------:|:-------------------------:|
|pin 1 | PA0  |  NO     |  AIN0      |  NO     |  UDPI/RESET[^4]           |
|pin 2 | N/A  | N/A     |  N/A       |  N/A    |  R-CFG[^5]                |
|pin 3 | PA3  | WO_3    |  AIN3      |  NO     |  ExtCLK                   |
|pin 4 | GND  | N/A     |  N/A       |  N/A    |  All Standoffs are GND    |
|pin 5 | VDD  | N/A     |  N/A       |  N/A    |  Voltage depends on R-CFG |
|pin 6 | PB0  | WO_0    |  AIN11     | X5/Y5   |  SCL                      |
|pin 7 | PA4  | WO_4    |  AIN4      | X0/Y0   |                           |
|pin 8 | PB1  | WO_1    |  AIN10     | X4/Y4   |  SDA                      |
|pin 9 | PA5  | WO_5    |  AIN5      | X1/Y1   |                           |
|pin 10| PB2  | WO_2    |   NO       |  NO     |                           |

Internal Pins:
Pin PC0: connected to VSRC. This can be used with ADC to check cap voltage when amulet switched on. It is disconnected from Vcap when amulet is switched off.

> Footnotes:
> - 1: TCA0[^1]
> - 2: ADC0[^2]
> - 3: Peripheral touch controller, only usable with Microchip's Qtouch libraries[^3]
> - 4: Exposed for external programming in case internal fails or isn't placed. Strongly Advised not to use, Connected internally to Tx/Rx of ch340e[^4]
> - 5: Connected to CFG on MAX17220 boost regulator[^5]
>

### MCU

### Energy Storage

### Boost Converter

### Charging


**Version 1.0** Have a micro USB Connector. And charge with a simple LDO with a fixed voltage of ~2.8V. This was a simple and cheap way to charge however lacked indication of charge state.

**Version 2.1+** Have a USB Type C connector. They use a Rohm CC/CV charging IC. I was lucky to come across this as it gave me the ability to set charge voltage, charge current and cut off current as well as have a Status led. Most charge controllers are made for very specific battery chemistries so having something as configurable as this was nice.


### Versions/Changes
- 1.0 First version Micro Usb, One batch exists in prototypes
    - known issue 50uA power drain while off

### Source Files
- []()

### Datasheets
-  []()


---
# Face plates
Face plates are the upper boards and where the art and circuits are meant to go. Most examples are simple LED blinkies but sensors and other components can and should be considered for design as well! The ones listed below I plan on selling in small quantities on my tindie once I can afford to build up stock. For now they're here as references and examples.

## Proto

Prototype your circuits for your own face plate or forgo the fab house and free wire your own!

* Configured Voltage:
* 

{{ gallery(id="") }}
