+++
title = "Pendants"
description = "Pendant form factor"
draft = true
weight = 3
sort_by = "weight"
+++

The Pendant series is the bigger sibling of the amulets. Not only is the physical size bigger but the 22 MCU pins are completely broken out to the faceplate, letting you get more creative with face plate design options.


## Common Specifications
* Size: 80mmx30mmx7.2mm
* Pins: 24 pin header
* Programming over USB
* Serial debug over USB
* Charging over USB
* Single 3.0V power rail

---
# Base boards

## P-Tiny90

{{ gallery(id="tiny90") }}

### Specifications
* MCU: ATTiny 3217
* Pins: All 24 pins broken out to the header
* Programming: CH342F USB-UART chip for UDPI over serial programming and serial debugging
* Single 3.0V rail
* Energy Storage: 1.4v 90F hybrid capacitor

The Tiny90 also uses a much larger hybrid capacitor than the amulets. Although the single cell limits it to being 1.4 volts, the capacitance of 90 Farads let it handle much more current than the 15F hybrid caps used in the amulets. the Hybrid cap can be charged at max 1.5A, however to make it compatible with high power usb 2.0 ports current is limited to 500mA by means of IC configuration. There is a 300mA max LDO that provides the 3.0V rail to the ATTiny for when usb is plugged in, which also filters the output of the PMIC when in backup mode.
 
 If overall power usage is kept low I've gotten pretty impressive results of pwm animated cycling an rgb led for 12 hours straight on a full charge. Estimated charging time is somewhere between 30 minutes to an hour, your mileage may vary, temperature does impact charge effectiveness.

---
## P-TinySolar
Currently a work in progress, more info will be provided upon prototype verification and testing.

### Specifications
* MCU: ATTiny 3217
* Pins: All 24 pins broken out to the header
* Programming: CH342F USB-UART chip for UDPI over serial programming and serial debugging
* Single .3.0V rail
* Energy Storage: 1.4v 90F hybrid capacitor

---
# Faceplates
The top outward facing boards.

### Ouroboros

The ouroboros has a body that makes up two capacitive scrolls, while the fangs connect to an RGB led controlled by pwm pins.

{{ gallery(id="oroborus") }}

 * LED: Common Anode RGB
 * Pins: PB0, PB1, PB2
 * Touch: PA4, PC0, PC1, PC2, PC3, PA5, PA6, PA7, PC5
---
### Khepri

This scarab has largely the same rgb led set up as the ouroboros however is packed with more touch surfaces. Each wing acts as a capacitive slider, while each main segment of the bottom acts as a touch button. Currently undergoing prototype verification

{{ gallery(id="khepri") }}

 * LED: Common Anode RGB
 * Pins: PB0, PB1, PB2
 * Touch: 