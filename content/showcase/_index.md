+++
title = "Anwaar Showcase"
description = "User made face plates and projects"
sort_by = "weight"
weight = 10
draft = false
+++

Right now this is a place holder for user showcase

>I made anwaar to see what others can do with it,
>If you end up making your own I want to see!
>
>If inclined, email me: chipperdoodles@protonmail.com
>
>Share pictures so I can put you own my websites showcase!
>If you do be sure to include:
>- a project name (or placeholder for me to call it)
>- a name or handle you want to be referred to as
>- pronouns (just helpful for me referring to you in the post)
>- and optionally one link you'd want to people to find you at. 
>  (i.e social media handle or personal blog site)
>- Optionally a brief summary if you want to share background on it
>  (made it for a friend, thought it would be cool, had this neat idea)