+++
title = "Face Plates"
description = "List of Generic Faceplate designs"
draft = false
weight = 7
sort_by = "weight"
+++

Face plates are the top boards and where the art/circuits are meant to go. Most examples are simple LED blinkies but sensors and other components can and should be considered for design as well! The ones listed below I plan on selling in small quantities on my tindie once I can afford to build up stock. For now they're here as references and examples.

--- 

## Amulet

### A-Proto

Prototype your circuits for your own face plate or forgo the fab house and free wire your own!

* Configured Voltages: (SEL=L)3.0V, (SEL=H)1.8V
* If you don't want to use configured voltages remove these resistors from the back of the board and either replace the resistor with desired value or use the breakout CFG pad to solder a tht resistor. Do not use a custom resistor without first removing soldered on smd resistors.

{{ gallery(id="proto") }}

### A-Pentagram

5 points 3 eyes, all love.

{{ gallery(id="pentagram") }}

 * LED: 3 Red
 * PINS: PB0, PB1, PB2
 * Touch (1616 only): PA4, PA5
 * Configured Voltage:  (SEL=L)3.0V, (SEL=H)1.8 1.8V

### A-SabbyTabby

An injury to one makes kitty unhappy.

{{ gallery(id="tabby") }}
 
 * LED: 2 Red
 * PINS: PB2
 * Touch (1616 only): No
 * Configured Voltage: (SEL=L)3.0V, (SEL=H)1.8

### A-DGUS (Don't Give up Skeleton)

Momento Mori but supportive.

{{ gallery(id="dgus") }}

 * LED: 2 White
 * PINS: PB0, PB2
 * Touch (1616 only): PB1 (skull is touch pad)
 * Configured Voltage: (SEL=L)3.0V, (SEL=H)2.8V

### A-TrAnsRights

Gender Anarchy. Be true to yourself.

{{ gallery(id="transrights") }}
 
 * LED: 1 Blue, 1 Pink, 1 White 
 * PINS: PB0, PB2, PB1
 * Touch (1616 only): PA4 Pad is zone fill between A and circle
 * Configured Voltage: (SEL=L)3.0V, (SEL=H)2.8V

---

## Pendant

### P-Proto

### P-Ouroboros

The ouroboros has a body that makes up two capacitive scrolls, while the fangs connect to an RGB led controlled by pwm pins.

{{ gallery(id="oroborus") }}

 * LED: Common Anode RGB
 * Pins: PB0, PB1, PB2
 * Touch: PA4, PC0, PC1, PC2, PC3, PA5, PA6, PA7, PC5

### P-Khepri

This scarab has largely the same rgb led set up as the ouroboros however is packed with more touch surfaces. Each wing acts as a capacitive slider, while each main segment of the bottom acts as a touch button. Currently undergoing prototype verification

{{ gallery(id="khepri") }}

 * LED: Common Anode RGB
 * Pins: PB0, PB1, PB2
 * Touch: 

---

## Slab

Series still needs to be finalized examples TBA

---

## Khamsa

### K-1616-NL

Attiny 1616 powered board with 6 leds and a light sensor meant for 'Nightlight' purposes

 * 6 cyan Leds
 * MCU: ATTINY 1616
 * Sensor: OPT-3001
 * PWM Pins: PB3, PB4, PB5, PA3, PA4, PA5
 * I2C Pins: PB0, PB1
 * Touch: No
 * Programming: UDPI header

---