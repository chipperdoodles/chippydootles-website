
+++
title = "Amulet"
description = "A nice little circle"
draft = false
weight = 2
sort_by = "weight"
+++

# Overview
The amulet family is a circular 36mm diameter design and the smallest of the Anwaar designs.

## Family Specifications
* Size: 36mm diameter, 7.2mm overall height
* MCU: Mcu location on Base Board
* Pins: Six pins for pwm, i2c, gpio
* Programming: Programming over USB
* Charging over USB

---

# Base Boards
Currently there is one official base board, the **A-TINY-15F**.

[A-TINY-15F is certified open source hardware](https://certification.oshwa.org/us002580.html)!!

[We're also on crowd supply!!](https://www.crowdsupply.com/chipperdoodles-chippydootles/anwaar) Please subscribe to stay in the loop for when we go live!


## A-TINY-15F
{{ gallery(id="15F") }}
KiCad renders of version 4.0

{{ gallery(id="boost") }}
(pictured version 1.0, 3.0 will be the CS version)

### Specifications
* MCU: ATTINY 1616
* Pins: Six pins for pwm, i2c, gpio
* Programming: WCH342F for UDPI over usb-uart programming and usb serial 
* Charging: Rohm CC/CV charge controller
* Energy Storage: 2.8V 15F hybrid capacitor
* VDD: 1.8V to 3.6V configurable VDD

### Pin Out

{{ image(id="anwaar-a-tiny-15f-pinout") }}

Download: [Original size PDF of pinout](A-Tiny-15F-Pinout.pdf)

Pinout for A-Tiny-15F 3.0

| Pin  | Port | PWM[^1] | Analog[^2] | PTC[^3] |          Other            |
|------|:----:|:-------:|:----------:|:-------:|:-------------------------:|
|pin 1 | N/A  | N/A     |  N/A       |  N/A    |  CFG1[^4]                 |
|pin 2 | N/A  | N/A     |  N/A       |  N/A    |  CFG3[^4]                 |
|pin 3 | PA3  | WO_3    |  AIN3      |  NO     |  ExtCLK                   |
|pin 4 | GND  | N/A     |  N/A       |  N/A    |  All Standoffs are GND    |
|pin 5 | VDD  | N/A     |  N/A       |  N/A    |  Voltage depends on R-CFG |
|pin 6 | PB0  | WO_0    |  AIN11     | X5/Y5   |  SCL                      |
|pin 7 | PA4  | WO_4    |  AIN4      | X0/Y0   |                           |
|pin 8 | PB1  | WO_1    |  AIN10     | X4/Y4   |  SDA                      |
|pin 9 | PA5  | WO_5    |  AIN5      | X1/Y1   |                           |
|pin 10| PB2  | WO_2    |   NO       |  NO     |                           |


Internal Pins:
Pin PC0: connected to VEN. This can be used with ADC to check cap voltage when amulet switched on. It is disconnected from Vcap when amulet is switched off.
PIN PA7: Connected to VSEL of buck-boost. Pulling this pin high Selects Voltage 1, pulling this pin low selects Voltage 2.

> Footnotes:
> - 1: TCA0[^1]
> - 2: ADC0[^2]
> - 3: Peripheral touch controller[^3]
> - 4: Connected to CFG1 and CFG3 on TPS63900 buck-boost regulator[^4]
>   

### MCU
The MCU on board is the ATTINY 1616. This is a very small 8-Bit AVR chip that can do a lot. These chips are very easy to work with and have decently low active power (when clocked accordingly). Thanks to a community made arduino core these are very easy to get programmed and started with.

It has a Touch Peripheral which allows for some fun designs however the caveat is that it relies on Microchips closed source (but free to use) Qtouch libraries. There is an experimental arduino library reverse engineered in progress so ptc may be possible in arduino! 

### Energy Storage
The energy storage is a 15 Farad Hybrid cap. As per the data sheet and application notes I limit CC charging to ~20mA and cut charging off when current reaches 5mA. Max discharge of the caps can be done safely up to 70mA however for long running times I advise average currents to be kept < 2mA. I've had pwm animated leds run from 6-8 hours depending on average current draw.

### Buck-Boost Converter
The TPS63900 is a new buck-boost converter from TI. It can output 1.8-5.0V and supports input current limiting as well as toggling between 2 different output voltages. On this base board the input current is limited to 50mA max to help protect the hybrid cap. Due to the way the resistor configuration works this leaves us with configurable output voltages of 1.8-3.6V possible. In concept, Toggling voltages is to switch to a lower voltage when the chip is sleeping or doesn't need a high voltage for leds or transmit power. The ATTINY has a wide vin so you can shave some power usage by switching to 1.8V while sleeping. If you do use capability this keep in mind the max/min voltage of any peripheral ICs/Sensors you may have in your face plate design.

Voltage selection is attached to pin PA7 on the mcu. Pull High for V-CFG1 and pull Low for V-CFG3.

### Charging
Charging is done over USB connector.

**Version 1.0** Have a micro USB Connector. And charge with a simple LDO with a fixed voltage of ~2.8V. This was a simple and cheap way to charge however lacked indication of charge state.

**Version 2.1+** Have a USB Type C connector. They use a Rohm CC/CV charging IC. I was lucky to come across this as it gave me the ability to set charge voltage, charge current and cut off current as well as have a Status led. Most charge controllers are made for very specific battery chemistries so having something as configurable as this was nice.

### Versions/Changes

**you can effectively ignore versions 1.0-2.1 as they were unreleased**

- 1.0 First version Micro Usb, One batch exists in prototypes
    - known issue 50uA power drain while off
    - Not released, some prototypes handed out
- 2.0 Usb type C prototype 
    - power switch changed from EN pin to direct VDD connection
    - updated to usb type c connector
    - Fixed power drain issue by adding a load switch ic between LDO and VCAP
    - Internally connected ADC pin changed to PC0
    - Not built
- 2.1 Charger update
    - Charge circuit changed
    - dedicated Rohm charger added with led charge status indication
    - Not released, some prototypes handed out
- 3.0 Buck-Boost Update
    - Boost IC changed to TPS63900 Buck-Boost
    - Pin1 changed to CFG1
    - Resistor values for face plate change to match new Buck-Boost
    - Intended release version
- 4.0 Uart Update
    - CH340E replaced with CH432F
    - TX/RX connections added with IC change
    - several resistors changed from 0402 to 0603 package
    - Enlarged test points
    - Fixed stencil layer for standoffs. 

### Source Files
- [https://gitlab.com/chippydootles/anwaar/a-tiny-15f](https://gitlab.com/chippydootles/anwaar/a-tiny-15f)

### Misc
- [Oshwa Certification](https://certification.oshwa.org/us002580.html)
- [Amulet CrowdSupply Campaign](https://www.crowdsupply.com/chipperdoodles-chippydootles/anwaar)

### Datasheets
- Buck-Boost Converter [TPS63900](https://www.ti.com/lit/ds/symlink/tps63900.pdf?ts=1692234643698&ref_url=https%253A%252F%252Fwww.ti.com%252Fproduct%252FTPS63900)
- Charge Controller [BD7163QWZ](https://fscdn.rohm.com/en/products/databook/datasheet/ic/power/battery_management/bd71631qwz-e.pdf)
- MCU [ATTINY1616](https://ww1.microchip.com/downloads/en/DeviceDoc/ATtiny1614-16-17-DataSheet-DS40002204A.pdf)
- Hybrid Capacitor [196 HVC ENYCAP](https://www.vishay.com/docs/28409/196hvc.pdf)
