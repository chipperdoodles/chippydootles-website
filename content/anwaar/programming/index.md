+++
title = "Programming"
description = "Programming"
draft = false
weight = 8
sort_by = "weight"
+++

# Base boards with AVR chips

Anwaar Base boards use the newer UDPI protocol. As I understand this is a proprietary protocol from Microchip that is serial communications based. It's a single wire protocol which greatly reduces the pin count and space needed for direct access. Thanks to a python tool and community effort it is very easy to program udpi over a serial channel, allowing me to design the base boards to be programmable without need of any buttons or bootloaders simply by adding a usb-uart chip and a couple passives.

As of time of writing the Amulet A-Tiny-15F and the Pendant P-DA-90F (unreleased) both have onboard usb-uart lines for programming so now external hardware is needed. Future releases of other families such as the slab where the mcu is on the face plate may require external hardware for programming.

## Arduino

All my testing and programming with arduino uses the wonderful megaTinyCore and DxCore by Spence Konde, visiting the repository will give you info on how to set up core and so much more valuable info. I love these cores because they are more based around the mcu itself and not any specific board. This gives great freedom to me as a board designer and makes it a little easier to understand how you are interacting with you mcu than through abstracted board names and pin numbers.

For those who know how to add a core to their arduino quick reference here is the board json: 

`http://drazzy.com/package_drazzy.com_index.json`

If you need more guidance, follow the link here [DxCore or megaTinyCore installation](https://github.com/SpenceKonde/DxCore/blob/master/Installation.md)

Arduino Cores used for AVR based Anwaar boards:
- [DxCore](https://github.com/SpenceKonde/DxCore/tree/master)
- [megaTinyCore](https://github.com/SpenceKonde/megaTinyCore)

These repositories contain a treasure trove of knowledge for working with the newer AVR chips and I highly recommend visiting them and browsing through their documentation.

### Notes for coding in Arduino
- Pins are referred to by their port number, example `PIN_PB1`
- Fuse settings are in the drop down, most defaults are fine but for reduced power consumption I almost always use 1Mhz clock option.
- You will need to select the appropriate MCU in the library:
    * A-TINY-15F uses the ATTINY 1616
    * P-DA-90F uses the AVRXXDA32 (xx is place holder where xx is amount of flash, i.e 32, 64, 128)





