+++
title = "Face plate design"
description = "Design info for Face Plates"
draft = false
weight = 6
sort_by = "weight"
+++

## WIP, consider this outdated but still useful, rewrite tba

Anwaar consists of several series of sandwich style printed circuit board assemblies, as of now these are the Amulet, Pendant, and Slab.

Commonalities summed up:
* Both come in two parts, The base board (bottom) and the face plate (top, outward facing).
* Between board height is 4.0mm
* Modular Faceplates are electrically and physically compatible with based boards for their respective family.
* I encourage any designs to keep compatibility however don't let that stop you from having fun, just make sure to mention incompatibility exceptions if you go this route


Anwaar as a project is meant to promote artistic use of 
printed circuit board design. It is also a way to quite literally have power flow through
symbols and this was the basis that I started with! In my designs I try to incorporate
the symbol/art with copper which will be part of a simple circuit. The simplest being an LED circuit.

I've made a couple KiCad templates to fast track designing your own faceplate.
For the art part of this guide we'll just assume you're using the Anwaar Amulet template.

[KiCad Templates](https://gitlab.com/chippydootles/anwaar/kicad-anwaar-templates)

Folders in template:
 * Art - contains files for artistic design
 * Docs - some more info

## Art Guide:

### Graphic Design Overview:
There's a couple approaches you can take, 
* Art first, circuit later
* Circuit First, Art Later

Regardless of your approach I suggest you doodle out your ideas to help shape what you want to look. Then I recommend the second approach, Circuit first. This allows you to better judge sizes of traces, location of connections, vias, components.

circuit first workflow:
* Design circuit in EESCHEMA, assign components then populate your pcb
* Layout your components and for complex designs even go as far as routing your PCB (note these prelim routes are guides which will be deleted)
* Plot your pcb as an SVG
* Import SVGs as a multilayer document in your art program
* Make a new top layer and use the lower svg tracks and outlines as a guide for your art
* Illustrate or Draw your art
* Export Art at correct size as a windows bitmap file (hide all non final art layers)
* Import Art using KiCad's built in footprint from image tool
* Edit created footprint in either the footprint editor or via text editor to move design to copper layer
* use placeholder library in KiCad and assign the art footprint in eeschema
* update PCB and place art accordingly

Note that very complex designs will slow KiCad down considerably, your mileage may very based on the computer you are using. I intend of doing a more step by step art tutorial eventually, maybe a video on youtube as well for now this page will just be a general overview.

## Circuit Design
The circuit design is entirely up to you but here I'll try to give you an overview of things to keep in mind and generalities. It's recommended to familiarize yourself with the different base boards, sizes, electrical capabilities and limitations of each.

### LED Choice:
I've provided footprints for 3 sizes of LED, 0402, 0603, 1206. Feel free to use your own footprints as well, keep in mind you may want to remove the default silkscreen markings. The size depends on your graphic design and your comfort level hand soldering the board. Forward voltage and resistor selection is critical to the run time. I try to aim to have the overall current be less than 1mA in general. I've been experimenting with shooting for .33 mA per led and that has yielded some decent results so far. Keep in mind most LED's are tested at 20mA so the listed forward voltage tends to also be higher in the data sheets. When running at a lower current forward voltage is usually also lower. I've run led's with a listed forward voltage of 3.0V at voltages from 2.8-2.6 at a very low current. Keep in mind the cap does have a self discharge rate so if the cap isn't at a full charge or sits for a while it will probably relax in voltage making you need a fresh charge to use with higher Vf diodes.

### Trace placement
Keep in mind that if you have unmasked copper part of the circuit it's basically an exposed wire, I tend to to make the more exposed designs a ground pad to limit shorting accidents. Also each mounting hole is connected to ground so the screws and the plated mounting holds will be part of the ground return, this is important to mind so you don't put positive side trace too close to them. 

### Top Component Placement
The outward facing components will look best without silkscreens and worked into copper design, keep in mind the pads and the plating. For example If you are hand soldering leds, a slip of the iron can get solder onto the decorative design, not the worst if you are using HASL or tin plating but will aesthetically ruin an ENIG plated design. On the other hand if using HASL (lead free I hope!), pads can be soldered onto and worked into the design nicely. On some of my personal prototypes I actually coat the exposed plating with solder to give it a raised feeling.

TODO: Picture examples

### Bottom Component Placement
Make sure you keep the Mounting holes and header locked and not moved in the KiCad template, this will ensure compatibility with anwaar base boards. Templates include keep out zones, these zones are for the inside facing components and represent where the hybrid caps will be from the base board. For boards with the 15F hybrid cap, very low profile components can be placed in the keep out zone,  It's about 0.3mm headroom give or take. Resistors should fit fine but some bulkier caps and ICs may not. For boards that use the 90F hybrid cap there is no headroom behind face plate. This prevents components from being placed In that area behind the faceplate. Another keep out area to keep in mind is the area around the mounting holes, if using a spacer there are kind of a square of area around the edge and around the mounting holes you will want to avoid. You also don't have to put resistors or components on the bottom side of a faceplate, if they work in your design feel free to get creative and move them on top.

TODO: picture of keep  out area

### Info on base boards

here's some overviews of the base boards electrical info 

#### A-TINY-15F
This board contains a boost converter that will boost the hybrid cap's output to a selected value based on the faceplates configuration resistor. This allows you to select a more appropriate output voltage to match with color/choice of diodes. For blues/whites I like to use 3.0V, for reds I'm still experimenting with 1.8v. Power usage will be easier to predict based off the fact that you will have a mostly constant voltage and color lighting throughout run time should be more consistent. 

* Voltage: regulated 1.8V -> 3.6V when boosting
* Current: keep below 5mA current draw if you can for longevity.
* Max Current discharge 15F hybrid caps support is 70mA but you will get barely any sustained life with that.
* 6 PWM capable pins, I2C and ADC functions available on those pins as well
* ATTiny 1616
* face plate designs need to have 2 cfg resistors.


