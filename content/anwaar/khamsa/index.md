+++
title = "Khamsa"
description = "Khamsa form factor"
draft = true
weight = 5
sort_by = "weight"
+++

# Overview 
Khamsas are uniquely shaped Boards where the base board is power only.

## Common Specifications
* Size: approximately 52mmx64mm
* Energy Storage: 2x 2.8V 15F hybrid cap
* USB-C for Charging with usb passthrough to face plate

# Programming
This series varies from the others in that the microcontroller is on the face plate, therefore a programming header or test points should be included in the top board. However USB data lines and power are passed through the header from the power board so if the mcu can be programmed over usb in any matter that option is available for design considerations. However if you need to program a boot loader the design must include at least test points for programming the face plate.

# Base Boards
There are a couple power boards available, the K-USB version and the K-Solar version. Both are under prototyping.

### K-USB
Very Simple board, uses LDO for charging and max17220 for power out.

{{ gallery(id="usb") }}

Unique specs:
 * Similar to 

### K-Solar
Solar and USB powered version and currently in design. Prototype verification in progress.

{{ gallery(id="solar") }}

Unique specs:
 * AEM10941 solar energy harvesting and management IC

## header
- pin 1: GND
- pin 2: GND
- pin 3: SEL
- pin 4: SEL
- pin 5: D-
- pin 6: VDD
- pin 7: D+
- pin 8: VDD
- pin 9: VUSB
- pin 10: VCAP

![image](k-header.png)

# Face plates
The top boards, unlike the pendant and amulet, have the mcu and the bottom boards are soley the energy source and management.

### K-1616-NL-BLUE

Attiny 1616 powered board with 6 leds and a light sensor meant for 'Nightlight' purposes

 * 6 cyan Leds
 * MCU: ATTINY 1616
 * Sensor: LTR-303
 * PWM Pins: PB3, PB4, PB5, PA3,PA4,PA5
 * I2C Pins: PB0, PB1
 * Touch: No
 * Programming: UDPI header

### K-Tiny

Simple board with ATTINY 3217, 5 touch pads, and 1 non addressable RGB LED WIP

{{ gallery(id="tiny") }}

 * 1 RGB Led
 * MCU: ATTINY 3217
 * Pins: PB0, PB1, PB2
 * Touch: PC4, PA6, PB4, PB5, PC2
 * Programming: UDPI header

### K-nRF

An attempt at making a circuit python compatible ble Khamsa front plate. WIP

{{ gallery(id="knrf") }}

 * 1 RGB Led
 * MCU: nrf52840
 * Peripheral: BME680 (gas, temp, pressure sensor), 32mb qspi memory, VEML7700 Light Sensor
 * Pins: 
 * Touch: 
 * Programming: SWD test points, USB DFU