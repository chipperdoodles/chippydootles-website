+++
title = "Anwaar ( أنوار )"
description = "Anwaar by Chipperdoodles"
sort_by = "weight"
weight = 1
draft = false
+++

# What is anwaar?
Anwaar (Arabic for a collection of lights) are a series of boards that I designed to encourage the merger of art and electrical design in personally meaningful ways. I want to give people a platform for creating their own as well as the confidence, skills and knowledge to do so. 

{{ image(id="anwaar") }}

## Anwaar Families
To date the anwaar families are:
- [Amulet](../amulet/) (First to launch, current version 3.0)
    - 36mm diameter Circle
    - [Check out the crowd supply campaign!](https://www.crowdsupply.com/chipperdoodles-chippydootles/anwaar)
- [Pendant](../pendant/) (WIP)
    - 80mm x 30mm rounded rectangle
- Slab (tba)
    - Solar Harvesting!
    - 36mm x 72mm door/headstone shape
- Khamsa (tba)
    - Solar Harvesting!
    - 52mm x 64mm 'khamsa' shape
- Amulet Plus (tba)
    - about the size of 1.5 x an amulet


## 2 Piece design
They all share a two piece design; the bottom known as the base board and the top known as the face plate. The bottom boards are dedicated to power management while the top boards for peripherals and design. Certain families contain the MCU on the base board with pins broken out via header to the top board while other families may just contain power management on the base boards to offer less constraints when designing your own faceplate.

## Portable
All base boards have a built in rechargeable energy storage. As of time of writing these are all powered by Vishay 196HVC hybrid capacitors. I was determined from the start not to use single use primary coin cells, or lithium based batteries. This provided a challenge but ultimately I settled on these hybrid caps and have been quite happy with them. That being said I'm always on the lookout for new energy storage tech so hopefully options open up as time goes on. Some advantages compared to li-po and li-on are: 

 * Safety: Non-hazardous with no shipping restrictions
 * Charge Speed: Faster overall charge speed
 * Charge Cycle life: rated for up to 100,000 charge cycles compared to the ~1k of li-polymer
 * Wider Functional Temperature range
 * Size: the unique low profile and overall sizes are thanks to these lil hybrid caps
 
 ## Open source hardware!
 I'm a huge fan of open source. I simply love sharing. I think Anwaar is a nifty Idea and really want to share it with others to see what they can make from it. All base board designs will be open source with a CERN OHL license. ~~I may try to self certify OSHW as well~~ [Amulets are OSHW certified!](https://certification.oshwa.org/us002580.html). They were designed using the FOSS software KiCad and face plate art was drawn in Krita. 

 Face plate KiCad templates are freely available to help speed faceplate designs.

 Some reference face plates may be open sourced however not all face plates I design will be. This is due to my artwork being on them as I do not wish for people to simply copy my artwork, but to create their own. As a compromise I may release some artwork free versions of source files if it's requested enough, or make simple open source reference face plates with artwork I am not partial to or is fan art stuff I have no intention to sell.

 ## And more!
 My goal is to build this as a platform that can be any combination used, modified and added to. There may be very neat energy storage options to in the future we could thrown on a new base board design, or an a different mcu to try out! My designs have been chosen based on what I think is a good starting point and am excited to see what the maker and art communities can do!
