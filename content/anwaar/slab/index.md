+++
title = "Slab"
description = "Slab/Door/Headstone Shaped"
draft = true
weight = 4
sort_by = "weight"
+++

# Overview
The slab, stemmed from wanting to make a more generic and less culturally attached version of the khamsa and turned into nicely sized board.

## Family Specifications
* Size: 72mm x 36mm x 7.2mm
* MCU: Mcu location on face plate
* Pins: USB Data, Usb Power, VDD, VCAP
* Programming: Dependant on face plate
* Charging: Charging over USB
* Single Power rail, jumper configurable 1.8 or 3.0 VDD.

---
### Pinout
{{ gallery(id="slabheader") }}

- pin 1: GND
- pin 2: GND
- pin 3: SEL
- pin 4: SEL
- pin 5: D-
- pin 6: VDD
- pin 7: D+
- pin 8: VDD
- pin 9: VUSB
- pin 10: VCAP
---

# Base Boards

## S-30-SOLAR

{{ gallery(id="s-30") }}


### Specifications
* Charging: USB & Solar
* Energy Storage: Two 2.8V 15F hybrid capacitors
* VDD: 3.0V (or 1.8V)

### Energy Storage

I opted for using two 2.8V 15F hybrid capacitors since they only take up a marginal amount more than using a single 4.2V 15F hybrid capacitor. This also has the net gain of a significantly longer run time at but at a higher monetary cost. 

### Boost Converter

This board the Max17220 boost converter. It can boost from a source of 0.8V-5.0V to an output of 1.8V-5V depending on the configuration. 

### Charging

**solar harvesting info**

Charging is also available over USB connector for those less sunny days.


### Versions/Changes
- 1.0 
    - Uses Max17220 module, 3.0V output only
- 2.0
    - TPS63900 module, jumper selectable 1.8 or 3.0V VDD

### Source Files
- []()

### Datasheets
- Boost Converter [MAX17220](https://datasheets.maximintegrated.com/en/ds/MAX17220-MAX17225.pdf)
- Solar Harvesting Controller [AEM10941]()
- USB Charge Controller [BD7163QWZ](https://fscdn.rohm.com/en/products/databook/datasheet/ic/power/battery_management/bd71631qwz-e.pdf)
- Anysolar SolarBit []()


---
# Face plates
Face plates are the upper boards and where the art and circuits are meant to go. Most examples are simple LED blinkies but sensors and other components can and should be considered for design as well! The ones listed below I plan on selling in small quantities on my tindie once I can afford to build up stock. For now they're here as references and examples.

## P-Habibi-NL

* MCU: ATTINY 1616
* Programming: 3 Pin header for UDPI
* Peripherals: VEML 7700 Light Sensor
* Six Pink LEDS

{{ gallery(id="") }}
