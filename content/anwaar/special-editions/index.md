+++
title = "Special Editions"
description = "special editions"
draft = true
weight = 6
sort_by = "weight"
+++

This section is for special editions of various Anwaar products. A special edition is a board that requires a different color soldermask/finish so they are sold as a complete matching assembly. However, they are still be electrically and physically compatible with the standard base boards of the corresponding series.

### Special Edition Amulets

#### Qalb - قلب
{{ gallery(id="qalb") }}

[Tindie store link](https://www.tindie.com/products/25112/)

I've been on a slow journey on learning arabic, this is the first language spoken by my mom's family and it's been a task long coming. In the process i've come to really appreciate the beauty of the language and I was very excited to incorporate some into my artwork.

Qalb or in arabic: قلب
is the word for heart, somewhere along the lines I had the idea to shape it into one, that idea progressed into making the heart actually beat and I don't think I could be happier with the result.

* LED: 3 Pink
* PINS: PB0, PB1
* Touch (1616 only): PA4
* Configured Voltage (boost only): 3.0v
---
#### Nazar
{{ gallery(id="nazar") }}

A simple take on the nazar (mati in greek), a ward for the evil eye if you catch it lookin.

* LED: 1 white
* PINS: PB0
* Touch (1616 only): none
* Configured Voltage (boost only): 3.0v

---
#### كول خرا
{{ gallery(id="kolkhara") }}


A little rude board for my friends

* LED: 1 RGB
* PINS: PB0,PB1,PB2
* Configured Voltage (boost only): 3.0V
---
## Special Edition Pendants

#### Habibi - حبيبي
{{ gallery(id="habibi") }}

animated gif: wip

If you have any arabic speaking friends or family chances are this is the one word you've heard! It's used in a variety of ways but when it comes to the root of the word it's all about love.

* LED: 6 Pink
* PWM: PB0, PB1, PB2, PA0, PA1, PA2
* Touch: In testing
