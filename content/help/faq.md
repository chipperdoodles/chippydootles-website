+++
title = "FAQ"
description = "Answers to frequently asked questions."
draft = false
weight = 30
sort_by = "weight"
+++

I don't actually have any frequently asked questions yet so uh, feel free to ask.

## What is this site?
This site is for documentation and information on anything I sell on my storefront or projects I may want to share and open source along the way.


## Contact the creator
Any questions about anything feel free to shoot me an email!
Send *Chipperdoodles (Martin)* an E-mail:
- <chipperdoodles@protonmail.com>
