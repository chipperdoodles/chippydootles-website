+++
title = "Some Helpful Links"
description = "Helpful Links"
draft = false
weight = 10
sort_by = "weight"
+++

## Chippydootles Repositories
- [Chippydootles Hardware](https://gitlab.com/chippydootles/chippydootles-hardware)
- [Anwaar Repo](https://gitlab.com/chippydootles/anwaar/)
- [Firmware Examples](https://gitlab.com/chippydootles/chippydootles-example-code)

## Software
- [KiCad](https://www.kicad.org/) I started my electronics journey in KiCad in 2017? It's only gotten better since
- [Krita](https://krita.org) FOSS digital art program, I use it along with a intous for hand drawing some raster art
- [FreeCad](https://www.freecadweb.org/) There can be a learning curve but it's a very capable parametric design CAD
- [Darktable](https://www.darktable.org/) Open source digital photo studio (alternative to adobe lightroom)
- [Affinity Designer](https://affinity.serif.com/en-us/designer/) A proprietary paid application for vector art but offers product at much more affordable price than Adobe
- [Zola](https://www.getzola.org/) Open source static-site generator written in rust
- [adidoks theme](https://www.getzola.org/themes/adidoks/) Zola Theme used for this site

## Fabhouses
- [Oshpark](https://oshpark.com/) Circuit board fab house based in the united states. Champion of the Pretty Purple PCB.
- [JLCpcb](https://jlcpcb.com/) Circuit board fab house based in china
- [Pcbway](https://www.pcbway.com/) Circuit board fab house also in china

## Digital Component Suppliers
- [Digikey](https://www.digikey.com/) My go to digital front for component sourcing
- [Mouser](https://www.mouser.com/) My second go to for component sourcing

## Arduino and maker related
- [Arduino](https://www.arduino.cc/) Educational based Programming tool for microcontrollers. Easiest way to get started for most chippydootles.
- [megaTinyCore](https://github.com/SpenceKonde/megaTinyCore) Arduino core for Attiny series 0,1, and 2 microcontrollers. Needed for Amulets and Pendant programming.
- [megaTinyCore owner's tindie](https://www.tindie.com/stores/drazzy/) Tindie store of the maintainer/owner of megatinycore.
- [Tindie](https://www.tindie.com/) Webfront for electronics makers to sell to others! Very neat.

## Chipperdoodles Related
- [My Webcomic](https://noties.org)
- [Hardware blog](https://noties.space)
- [Hackadayio Profile](https://hackaday.io/chipperdoodles)
- [Mastodon](https://chaos.social/@chipperdoodles)
