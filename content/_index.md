+++
title = ""
description = "The Introduction"
sort_by = "weight"
weight = 1
+++

## Welcome! 
If you purchased any of my electronics (aka doots) from me (THANK YOU IF YOU DID!! wow!!), are just curious what they are,
or want to build one yourself you have come to the right place!

### What's Anwaar?
Anwaar is big project of mine meant to serve as a platform for personal portable pcb tokens
* [Anwaar](/anwaar/anwaar-overview)

### Misc Dev boards
These are boards meant to serve as development tools. They include breakout boards and programmers.
* [Dev Boards](/dev_boards/)

### Keyboards
I started my electronics journey in keyboards and will have some designs to share (tba)


### Help
- [FAQ](/help/faq/)
- [Programming Notes](/anwaar/programming/)
- [Handy Links](/help/links)
