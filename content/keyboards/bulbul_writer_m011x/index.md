+++
title = "Bulbul Writer m011x"
description = "Modern replacement PCB for m0116/8 Apple Standard Keyboard"
sort_by = "weight"
weight = 2
draft = false
+++

## Status
**designed but not tested**

## Links
* [Hardware Source Code](https://gitlab.com/chippydootles/keyboards/bulbul-m0118-340)

## Overview
the bulbul writer project is similar to project plapple however much simpler. The m0116/8 keyboards used alps switches and PCBs making a pcb retrofit easy. This project can reuse every piece of the old m0116/8 except for the PCB (which would be replaced). The USB ports are set up with a mux meaning only one can be used at a time but it at least allows you to choose which side.

## Specs
* mcu: nrf52840
* firmware: zmk
* switch footprints: alps only
* battery: default configured for 10440 (~AAA) LiFePo4
* layout: compatible with either m0116 or m0118 layout
* peripherals: FFC connector for MIP sharp display
* VDD: 3.0V
