+++
title = "Project Plapple"
description = "PCB replacement for apple Keyboard II case"
sort_by = "weight"
weight = 1
draft = false
+++

## Status
**most recent version designed but not tested**

## Links
* [Hackadayio project page](https://hackaday.io/project/188239-codename-project-plapple)
* [Hardware Source Code](https://gitlab.com/chippydootles/keyboards/plapple340))

## Overview
This project was my first keyboard replacement project. The apple keyboard II is an apple ADB keyboard from the early to mid 90s. There were two manufacturers smk and mitsumi however both versions are membrane based. This is a unique project because it sought to replace a membrane keyboard's internals with a simple mechanical keyswitch based pcb. Basically it involves gutting the apple keyboard II and using the case. It can also be used standalone with it's own case, simple 2 piece switch plate case drawings are provided in the source files.

## Specs: 
* mcu: nrf52840
* firmware: zmk
* switch footprints: Kailh hot swap sockets with optional solderable mx/alps footprint as well
* battery: configured for 10440 LiFePo4
* layout: standard 60% with numpad (non standard numpad)
* peripherals: I2c connector header
* VDD: 3.0V
