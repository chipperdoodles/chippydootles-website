+++
title = "Face plate Assembly"
description = "tips for at home assembly"
draft = true
weight = 40
sort_by = "weight"
+++

Assembling your own face plate may seem daunting, depending on the size of components used soldering skills may need to be at least intermediate.

# Freehand with solder iron
- the header

# Paste and stencil
 - Hand pasting Jigs
    A trick I learned at work is using PCBs to help form soldering jigs. This works if you have a bunch of extra pcbs and is nice because the pcbs are the same height which helps keep the stencil on a level area.

- 
