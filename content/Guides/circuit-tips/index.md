+++
title = "Anwaar Circuit Tips"
description = "Tips for Designing your Anwaar circuit"
draft = false
weight = 20
sort_by = "weight"
+++

# EE Tips

Some general advice for the electronic aspect of circuit designs and things to consider for your own Anwaar faceplate designs.

## Hybrid Cap approximate runtimes

I was looking at the datasheet and trying to interpret their backup times table. I may have read it wrong but in general here are datasheet values backup times.

15F (used on the amulet):
- ~17 minutes(1,000s) @ 10mA
- ~2.9 hours(105,00s) @ 1mA
- ~5.56 hours(20,000s) @ 500uA
- ~28 hours(100,000s) @ 100 uAmps

90F( used on the pendant):
- ~2.8 hours (10,000s) @ 10mA
- ~8.33 hours(30,000s) @ 5mA
- ~27.7 hours(100,000s) @ 2mA

Keep in mind these are based on current load at the CAP, since we're boosting and bucking the cap the current draw will increase as the caps discharge and also heavily depend on output VDD. The 90F cap in the pendant is 1.4V and output VDD is 3.0Vs so current drawn on the cap will ~1.5-twice as much as what you measure for your circuit @3v.

## Led brightness and current

Regardless of the family selected these are meant to be very low power designs. To get the longest run time any amount of energy savings is important. It's no surprise that lighting LEDs is one of the most costly bits of energy so reducing their current draw is a priority to get longer run times. 

### Led Series Resistors 
In general you want to select series resistors that really choke the current. I tend to default to 1k for the series led resistor. Keep in mind this affects visibility in bright areas but these should be considered for indoors and nighttime uses. You don't need a lot of current to make leds very noticeable/bright in lower light conditions, I have some designs that keep leds to a maximum of ~1 mA and it's still sufficient.

### Forward Voltage
Diodes of different colors require different voltage thresholds in order to light up. These are often referred to as vF or the Forward voltage. You can think of it as the voltage required for the current to move 'forward' across the diode and light up. One less obvious thing is that forward voltage and current are tied together. There's still a needed amount of voltage under which your diode won't light up but diode data sheets usually list a higher forward voltage for whatever current that led series was tested at. This can be as high as 20mA! In general the lower current you use the lower the vF will be to a point. For example a diode with a vF of ~3 volts can be lit up with voltages down to ~2.8v. A caveat is There may be a noticeable difference in brightness and it usually takes trial and error to find the sweet spot of least current and lowest voltage vs acceptable brightness. So in this case you can treat the vF they give you as a guideline as compared to a hard number to stick with.

**Specific to amulets:** 
    The amulets have a neat buck boost converter. The design lets you set 2 different voltages with 1 resistor respectively and then be able to toggle between the two by pulling the select pin high or low. The faceplates are designed to take advantage of this and let the designer set these voltages according to their needs. In this case they can best match the VDD of the amulet to the vF needed for the leds used on the faceplate. This gives you the potential of things like toggling between different maximum brightnesses or using a low voltage while the leds are not lit to save even more power. 

Keep in mind if you use different colors you will need to design for the voltage of the led with the highest vF (typically blue). To balance this out you will need a higher resistor on red to help balance it's brightness compared to the blue.

### PWM
PWM (pulse width modulation) can be used to cut down on led's power draw by rapidly switching the led off and on. Certain duty cycles can achieve near full brightness to the human eye without actually keeping the led on 100% of the time. In addition to that PWM is the main method used for animating leds.

**Specific to amulets:** 
    Amulets break out 6 gpio from the ATTINY1616. All 6 of these by default (if using megaTinyCore for arduino) can output 8-bit pwm waveforms. However you can use 16-Bit pwm if only using the first three pins (and configuring pwm settings in your firmware). This gives you a higher resolution to play with at cost of pincount. Note that you must use specific pins if wanting to do this so keep that in mind when designing. These pins are PB0, PB1, PB2.

## Sleep and unused Pins

In terms of current savings you can get a lot from sleeping. Using sleep can greatly enhance your overall runtime. The megatinycore has an excellent section on setting up [sleep and power savings using the core](https://github.com/SpenceKonde/megaTinyCore/blob/master/megaavr/extras/PowerSave.md).

**Specific to amulets:** 
    It's very important when setting unused pins to output to **NOT** set pin PC0 to output. This is tied to VCAP/VEN so a user can read VCAP with the adc of the attiny. enabling it as an output leads to high current draw and weirdness so please make sure to leave that pin floating if not used and analog input if used.

## Touch

Thanks to an [experimental library](https://github.com/MX682X/ptc_touch) touch is available on ATTINY 1 series based base boards. This lets you use the peripheral touch controller to make touch buttons with little to no external circuitry. You may want to add some safety resistors but in my test designs I left them off for space saving. In terms of physical design just keep in mind the size of fingertips to help judge how close you can have touch buttons next to each other. Too small a pad and close usually means triggering both buttons with a single touch. I've only done some basic experiments with the library but it does appear to give you basic access to the touch peripheral.

## MCU clock Speed

Quite simply for most MCUs the higher the clock speed the more power drawn. For your circuit you will have to judge the pros/cons of this. Most of my sketches run at 1MHZ to save power and it works really well for simple PWM animations and keeps current draw low. 