+++
title = "KiCad Template usage"
description = "How to use the KiCad templates"
draft = false
weight = 10
sort_by = "weight"
+++
# KiCad Templates

I made face plate templates for KiCad. Templates are a great tool for speeding up design for accessories to various boards.

To use a template simply simply clone or [download the repository](https://gitlab.com/chippydootles/anwaar/kicad-anwaar-templates) and either move the folders over to your KiCad user templates directory, or select the appropriate path in KiCad.

Make sure each individual template folder is in the root of your set templates folder. Nested folders will not show up in Kicad.
![](./templatesfolder.png)

## Getting started with the template
1. Open KiCad and select File > New project from template. (shortcut CTRL + T)

2. Either Open template path, or Navigate to User templates and select the template

    ![](./usertemplateskicad.png)

3. Select location you want project to save/project name

## The schematic
I tried my best to make the schematic as informative as I can while providing parts to serve as a sort of demo.

From here It's up to you! Feel free to delete all the information you don't need, and change the document info etc. This is your file now, go for it! Don't forget to edit the sheet information with your project name and author/date info for both the schematic and PCB.

If you are deleting parts make sure to not remove the header or the standoffs, these parts should be locked on the pcb design view as they are meant to line up with the base board. You can remove the configuration resistors if you short both CFG3 and CFG1 to ground which will give you 1.8V in either VSEL direction. My advice is to keep them on the footprint unless you know for sure you want 1.8V. Note that these are simply guidelines, feel free to get as wild as you want with shapes. You can even delete the standoffs if you don't want to screw down the board or for example if you only want 3 of them instead of the 5 to give you more design room on the face plate.
