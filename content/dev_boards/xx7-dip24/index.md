+++
title = "Attiny x7 dip24 Breakout"
description = "Attiny dip sized breakout"
draft = true
weight = 1
sort_by = "weight"
+++

# Overview
Attiny x17 and x27 Bare bones breakout boards for prototyping with no assumptions about power management, programming etc.

### Specifications
* Size: 
* MCU: Attiny xx7, (x27 or x17)
* Pins: All Pins broken out with two 1x12 headers
* Programming: UDPI pin exposed on in pinout

{{ gallery(id="dip") }}



