+++
title = "Amulet Udpi Helper"
description = "Amulet Programmer and Breakout board"
draft = true
weight = 2
sort_by = "weight"
+++

# Overview
This design is a digitally isolated udpi over serial programmer as well as a 10 pin breakout board for the noor martin amulet baseboard. This is to allow udpi over uart programming as well as a handy breakout if the user wants to prototype their own faceplate before designing one. I made these before I added on board programming to the amulets when there was a need for a separate programmer. These can still be useful as breakouts and I might make some, but I'm just leaving them up for reference.


{{ gallery(id="amulet-udpi-friend") }}





