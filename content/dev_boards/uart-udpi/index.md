+++
title = "Uart-UDPI Helper"
description = "simple adaptor for udpi serial programming"
draft = false
weight = 2
sort_by = "weight"
+++

# Overview
A simple board to act a a solution to turn common footprint usb-uart tools into a udpi over serial programmer. [Megatinycore's design guide was used as a reference]("https://github.com/SpenceKonde/AVR-Guidance/blob/master/UPDI/jtag2updi.md"). This board was designed to be simple and able to diy assemble.

{{ gallery(id="uart-udpi") }}

### Usage
this is simply meant to act as an in between your common footprint serial adaptor and your programming target. Can be used with megatinycore in arduino or standalone with ![Microchip's tool]("https://github.com/microchip-pic-avr-tools/pymcuprog")

USB-UART --> Helper --> target.

### VCC jumper (important!!)
Please be very careful here, by default the jumper is open so that the power of the source is disconnected from the power of the target. This is because most of the use cases for my portable boards have their own power source. The jumper is there in case you need to provide power to the target from your usb-uart adaptor. It's not too complicated but I ask to leave this open unless you know what you're up to.

### Special provisions for programming amulets
You can largely Ignore these but they're there as a just in case option. They were put there for programming old amulets before the pinout changed and removed the UDPI pin from the header.

On the bottom of the board there are two unpopulated footprints. One is for a male smd 1.27mm pitch 02x05 connector used for connecting and programming the attiny on an amulet base board, the other is for a resistor. This resistor is meant to be the voltage configuration on the A-TINY-15F boards, Take note that, while the cfg resistor is sandwiched neatly under the through hole resistor they do not share net connections and should not be bridged.


