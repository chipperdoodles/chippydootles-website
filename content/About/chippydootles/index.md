+++
title = "About"
description = "About Chipperdoodles"
draft = false
weight = 1
sort_by = "weight"
+++

## About me
Chipperdoodles (They/Them) is a rather silly person.They've had a varied life that was never one thing or the other, just one foot in and one foot out. They grew up on citrus ranches in California but went to school in the suburbs. They studied and worked in print for 7 years at university and daydreamed about creating comics so they actually making their own webcomic. Then eventually they came back home home and worked in the ranch mechanic shop. Somewhere a long the line they picked up a keyboard hobby which turned into an electronic design hobby. They got a job as a pcba machine operator which they worked for 4 years before learning that they are wholly incompatible with corporate culture. That brings them to now and the creation of Chipperdoodles Chippydootles, a place they can share their electronic creations with people.

## About Roland
Roland is the official spokes-monster of Chippydootles and loves to help chipperdoodles tell people about things.

{{ image(id="roland") }}

## About Chippydootles
In terms of an actual storefront. I have a Tindie set up. I have some stock built up for minor things but it hasn't been a focus to push on due to some attention needed elsewhere. Currently working on a crowd supply campaign to launch the first of the Anwaar doots has been the focus with hopes that I can stock face plates in my tindie store but I wouldn't want to make those available until actual amulet baseboards are available. I hope to be able to make some money to be able to support prototyping more chippydootles and providing more Anwaar base boards as well as other projects. I would gladly give doots away if I could but sadly that is an unreality so I will try to do my best with what I've got.